all: tc

parse_tree.o: parse_tree.o
	cc parse_tree.c -c

lex.yy.c: parse_tree.o
	lex analyze.lex

y.tab.c: lex.yy.c
	yacc -d analyze.y

tc: y.tab.c
	cc -o tc parse_tree.o y.tab.c lex.yy.c -ly -ll

clean:
	rm y.* && rm lex.yy.c && rm tc && rm parse_tree.o
