%{
#include "tokens.h"
#include "y.tab.h"

int token_count = 0;
%}

%option yylineno

%%

"/*"([^*]|\*+[^*/])*\*+"/" { /*ignore comments*/ }

"let" {
  t = t_let;
  return T_LET;
}

"end" {
  t = t_end;
  return T_END;
}

"int" {
  t = t_integer;
  return INTEGER;
}

"string" {
  t = t_string;
  return STRING;
}

"float" {
  t = t_float;
  return FLOAT;
}

"boolean" {
  t = t_boolean;
  return BOOLEAN;
}

"for" {
  t = t_for;
  return FOR;
}

"to" {
  t = t_to;
  return TO;
}

"break" {
  t = t_break;
  return BREAK;
}

"true" {
  t = t_true;
  return TRUE;
}

"false" {
  t = t_false;
  return FALSE;
}

"var" {
  t = t_var;
  return VAR;
}

"function" {
  t = t_function;
  return FUNCTION;
}

"if" {
  t = t_if;
  return IF;
}

"then" {
  t = t_then;
  return THEN;
}

"else" {
  t = t_else;
  return ELSE;
}

"while" {
  t = t_while;
  return WHILE;
}

"do" {
  t = t_do;
  return DO;
}

"in" {
  t = t_in;
  return T_IN;
}

[0-9]+  {
  t = t_num;
  strncpy (token_data[token_count++], yytext, TAM_TOKEN);
  return NUM;
}

[a-zA-Z][a-zA-Z0-9_]* {
  t = t_id;
  strncpy (token_data[token_count++], yytext, TAM_TOKEN);
  return ID;
}

":=" {
  t = t_attr;
  return ATTR;
}

":" {
  t = t_col;
  return COL;
}

";" {
  t = t_scollum;
  return SCOLLUM;
}

"," {
  t = t_comma;
  return COMMA;
}

"(" {
  t = t_oparenthesis;
  return OPARENTHESIS;
}

")" {
  t = t_cparenthesis;
  return CPARENTHESIS;
}

"+" {
  t = t_sum;
  return SUM;
}

"-" {
  t = t_minus;
  return MINUS;
}

"*" {
  t = t_mul;
  return MUL;
}

"/" {
  t = t_div;
  return DIV;
}

"=" {
  t = t_eq;
  return EQ;
}

"<>" {
  t = t_diff;
  return DIFF;
}

">" {
  t = t_greater;
  return GREATER;
}

"<" {
  t = t_less;
  return LESS;
}

">=" {
  t = t_greater_equal;
  return GREATER_EQUAL;
}

"<=" {
  t = t_less_equal;
  return LESS_EQUAL;
}

"&" {
  t = t_and;
  return AND;
}

"|" {
  t = t_or;
  return OR;
}

L?\"(\\.|[^\\"])*\" {
  t = t_literal;
  strncpy (token_data[token_count++], yytext, TAM_TOKEN);
  return LITERAL;
}

.|\n    {   /* Ignore all other characters. */   }
%%