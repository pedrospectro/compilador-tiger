%{
    #include "tokens.h"
    int count_t = 0;
%}

%error-verbose
%token VAR FUNCTION IF THEN ELSE DO WHILE FOR
%token T_LET T_IN T_END TO BREAK
%token NUM ID LITERAL
%token ATTR SCOLLUM COMMA OPARENTHESIS CPARENTHESIS
%token SUM MINUS MUL DIV
%token EQ DIFF GREATER LESS GREATER_EQUAL LESS_EQUAL
%token OR AND
%token TRUE FALSE
%token COL
%token INTEGER BOOLEAN STRING FLOAT

%%

program:    line_list   { node *n = create_node("<PROGRAM>"); insert_node(n, $1); tr = create_parse_tree(); tr->root = n; show_parse_tree(); }
    |       let         { node *n = create_node("<PROGRAM>"); insert_node(n, $1); tr = create_parse_tree(); tr->root = n; show_parse_tree(); }
;

epsilon: { node *n = create_node("EPSILON"); $$=n; }
;

exp_block:  line { node *n=create_node("<EXP_BLOCK>"); insert_node(n,$1); $$=n; }                          
    |       OPARENTHESIS line_list CPARENTHESIS { node *n=create_node("<EXP_BLOCK>"); insert_node(n,create_node("(")); insert_node(n,$2); insert_node(n,create_node(")")); $$=n; }
;

line_list:  line { node *n = create_node("<LINE_LIST>"); insert_node(n, $1); $$=n; }
        |   line line_list { insert_node($2, $1); $$=$2; }
;

line: command { node *n = create_node("<LINE>"); insert_node(n, $1); $$=n; }
    | command SCOLLUM { node *n = create_node("<LINE>"); insert_node(n, $1); $$=n; }
;

command:    attrib   { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }       
    |       var_declare { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }    
    |       if_then  { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }       
    |       if_then_else { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }   
    |       while     { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }      
    |       for         { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }    
    |       function_call { node *n = create_node("<COMMAND>"); insert_node(n, $1); $$=n; }  
;

exp:    expression     { node *n = create_node("<EXP>"); insert_node(n, $1); $$=n; }         
    |   logical_expression  { node *n = create_node("<EXP>"); insert_node(n, $1); $$=n; }    
    |   relation_expression  { node *n = create_node("<EXP>"); insert_node(n, $1); $$=n; }   
;

expression: expression SUM term  { node *n = create_node("<EXPRESSION>"); node *child=create_node("<+>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }   
    |       expression MINUS term  { node *n = create_node("<EXPRESSION>"); node *child=create_node("<->"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; } 
    |       term { node *n = create_node("<EXPRESSION>"); insert_node(n, $1); $$=n; }                   
;

term:   term MUL factor { node *n = create_node("<TERM>"); node *child=create_node("<*>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }
    |   term DIV factor { node *n = create_node("<TERM>"); node *child=create_node("</>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }
    |   factor { node *n = create_node("<TERM>"); insert_node(n, $1); $$=n; }
;

factor:     NUM { node *n = create_node("<FACTOR>"); insert_node(n, create_node(token_data[count_t++])); $$=n; }
    |       id_or_function_call { node *n = create_node("<FACTOR>"); insert_node(n, $1); $$=n; }
    |       OPARENTHESIS expression CPARENTHESIS { node *n = create_node("<FACTOR>"); insert_node(n, create_node("(")); insert_node(n, $2); insert_node(n, create_node(")")); $$=n; }
;

logical_expression:     logical_expression AND logical_factor   { node *n = create_node("<LOGICAL_EXPRESSION>"); node *child=create_node("<AND>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }
    |                   logical_expression OR logical_factor { node *n = create_node("<LOGICAL_EXPRESSION>"); node *child=create_node("<OR>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }    
    |                   logical_factor { node *n = create_node("<LOGICAL_EXPRESSION>"); insert_node(n, $1); $$=n; }
;

logical_factor:     TRUE { node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, create_node("true")); $$=n; }
    |               FALSE { node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, create_node("false")); $$=n; } 
    |               id_or_function_call {  node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, $1); $$=n;}
    |               OPARENTHESIS logical_expression CPARENTHESIS { node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, create_node("(")); insert_node(n, $2); insert_node(n, create_node(")")); $$=n; }
    |               relation_expression { node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, $1); $$=n; }
    |               expression { node *n = create_node("<LOGICAL_FACTOR>"); insert_node(n, $1); $$=n; }
;

relation_expression:    relation_expression LESS relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<LESS>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n; }           
    |                   relation_expression LESS_EQUAL relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<LESS_EQUAL>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n;  }      
    |                   relation_expression GREATER relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<GREATER>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n;}         
    |                   relation_expression GREATER_EQUAL relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<GREATER_EQUAL>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n;}   
    |                   relation_expression EQ relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<EQ>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n;}              
    |                   relation_expression DIFF relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); node *child=create_node("<DIFF>"); insert_node(child, $1); insert_node(child, $3); insert_node(n, child); $$=n;}            
    |                   relation_factor { node *n = create_node("<RELATION_EXPRESSION>"); insert_node(n, $1); $$=n; }                                     
;

relation_factor:    NUM { node *n = create_node("<RELATION_FACTOR>"); insert_node(n, create_node(token_data[count_t++])); $$=n; }
    |               id_or_function_call { node *n = create_node("<RELATION_FACTOR>"); insert_node(n, $1); $$=n; }
    |               OPARENTHESIS relation_expression CPARENTHESIS { node *n = create_node("<RELATION_FACTOR>"); insert_node(n, create_node("(")); insert_node(n, $2); insert_node(n, create_node(")")); $$=n; }
    |               logical_expression { node *n = create_node("<RELATION_FACTOR>"); insert_node(n, $1); $$=n; }
    |               expression { node *n = create_node("<RELATION_FACTOR>"); insert_node(n, $1); $$=n; }
;

type:   INTEGER { node *n=create_node("<TYPE>"); insert_node(n,create_node("INTEGER")); $$=n; } 
    |   FLOAT { node *n=create_node("<TYPE>"); insert_node(n,create_node("FLOAT")); $$=n; }   
    |   BOOLEAN { node *n=create_node("<TYPE>"); insert_node(n,create_node("BOOLEAN")); $$=n; } 
    |   STRING { node *n=create_node("<TYPE>"); insert_node(n,create_node("STRING")); $$=n; }  
;

fields: field_declare { node *n=create_node("<FIELDS>"); insert_node(n,$1); $$=n; }
    |   epsilon { node *n=create_node("<FIELDS>"); insert_node(n,$1); $$=n; }
;

fields_call:    field_call_declare { node *n = create_node("<FIELDS_CALL>"); insert_node(n,$1); $$=n; }
    |           epsilon { node *n = create_node("<FIELDS_CALL>"); insert_node(n,$1); $$=n;}
;

field_call_declare:     id_or_function_call { node *n = create_node("<FIELDS_CALL_DECLARE>"); insert_node(n,$1); $$=n; }
            |           NUM { node *n = create_node("<FIELDS_CALL_DECLARE>"); insert_node(n,create_node(token_data[count_t++])); $$=n;}
            |           LITERAL { node *n = create_node("<FIELDS_CALL_DECLARE>"); insert_node(n,create_node(token_data[count_t++])); $$=n;}
            |           id_or_function_call COMMA field_call_declare { insert_node($3,$1); $$=$3; }
            |           NUM COMMA field_call_declare { insert_node($3,create_node(token_data[count_t++])); $$=$3;}
            |           LITERAL COMMA field_call_declare { insert_node($3,create_node(token_data[count_t++])); $$=$3; }
;

field_declare:  id COL type { node *n=create_node("<FIELD_DECLARE>"); insert_node(n,$1); insert_node(n,create_node(":")); insert_node(n,$3); $$=n;}
            |   id COL type COMMA field_declare { insert_node($5,$1); insert_node($5,create_node(":")); insert_node($5,$3); $$=$5; }
;

function_declare:   FUNCTION id OPARENTHESIS fields CPARENTHESIS EQ function_body { node *n=create_node("<FUNCTION_DECLARE>"); insert_node(n,create_node("FUNCTION")); insert_node(n,$2); insert_node(n,create_node("(")); insert_node(n,$4); insert_node(n,create_node(")")); insert_node(n,create_node("=")); insert_node(n,$7);  $$=n; }          
    |               FUNCTION id OPARENTHESIS fields CPARENTHESIS COL type EQ function_body { node *n=create_node("<FUNCTION_DECLARE>"); insert_node(n,create_node("FUNCTION")); insert_node(n,$2); insert_node(n,create_node("(")); insert_node(n,$4); insert_node(n,create_node(")")); insert_node(n,create_node(":")); insert_node(n,$7); insert_node(n,create_node("=")); insert_node(n,$9);  $$=n; } 
;

function_body:  NUM { node *n=create_node("<FUNCTION_BODY>"); insert_node(n,create_node(token_data[count_t++])); $$=n; }
            |   exp_block { node *n=create_node("<FUNCTION_BODY>"); insert_node(n,$1); $$=n;}
;

function_call: id OPARENTHESIS fields_call CPARENTHESIS { node *n = create_node("<FUNCTION_CALL>"); insert_node(n,$1); insert_node(n, create_node("(")); insert_node(n,$3); insert_node(n, create_node(")")); $$=n; }
;

id_or_function_call:    id { node *n = create_node("<ID_OR_FUNCTION_CALL>"); insert_node(n, $1); $$=n;  }
                |       function_call { node *n = create_node("<ID_OR_FUNCTION_CALL>"); insert_node(n, $1); $$=n; }
;

var_declare:    VAR var_list { node *n = create_node("<VAR_DECLARE>"); insert_node(n, $2); $$=n;  }
            |   VAR attrib { node *n = create_node("<VAR_DECLARE>"); insert_node(n, $2); $$=n; }
;

var_list:   id { node *n = create_node("<VAR_LIST>"); insert_node(n, $1); $$=n;  }
        |   id COMMA var_list { insert_node($3, $1); $$=$3; }
;

attrib:     id ATTR exp { node *n = create_node("<ATTR>"); insert_node(n, $1); insert_node(n, $3); $$=n; }
        |   id ATTR literal { node *n = create_node("<ATTR>"); insert_node(n, $1); insert_node(n, $3); $$=n;}
;

id: ID { $$ = create_node(token_data[count_t++]); }
;

literal: LITERAL { $$ = create_node(token_data[count_t++]); }
;

if_then:    IF exp THEN exp_block { node *n=create_node("<IF_THEN>"); insert_node(n,$2); insert_node(n, create_node("THEN")); insert_node(n, $4); $$=n; }
;

if_then_else:     IF exp THEN exp_block ELSE exp_block { node *n=create_node("<IF_THEN_ELSE>"); insert_node(n,$2); insert_node(n, create_node("THEN")); insert_node(n, $4); insert_node(n, create_node("ELSE")); insert_node(n, $6); $$=n; }
;

while:  WHILE exp DO exp_block { node *n=create_node("<WHILE>"); insert_node(n,$2); insert_node(n, create_node("DO")); insert_node(n, $4); $$=n; }
;

for:  FOR id ATTR exp TO exp DO exp_block { node *n = create_node("<FOR>"); insert_node(n, $2); insert_node(n, create_node("ATTR")); insert_node(n, $4); insert_node(n, create_node("TO")); insert_node(n, $6); insert_node(n, create_node("DO")); insert_node(n, $8); $$=n; }
;

let:    T_LET decs T_IN line_list T_END { node *n = create_node("<LET>"); insert_node(n, create_node("T_LET")); insert_node(n, $2); insert_node(n, create_node("T_IN")); insert_node(n, $4); insert_node(n, create_node("T_END")); $$=n; }
;

decs:   dec { node *n = create_node("<DECS>"); insert_node(n, $1); $$=n; }
    |   epsilon { node *n = create_node("<DECS>"); insert_node(n,$1); $$=n; }
    |   dec decs { insert_node($2, $1); $$=$2; } 
;

dec:    var_declare { node *n = create_node("<DEC>"); insert_node(n, $1); $$=n; }
    |   var_declare SCOLLUM { node *n = create_node("<DEC>"); insert_node(n, $1); $$=n; }
    |   function_declare SCOLLUM { node *n = create_node("<DEC>"); insert_node(n, $1); $$=n; }
    |   function_declare { node *n = create_node("<DEC>"); insert_node(n, $1); $$=n; }
;

%%

int main (void) { yyparse(); }

void yyerror(char *s){
    extern int yylineno;
    fprintf (stderr, "%s on line %d\n", s, yylineno);
}
