#include "parse_tree.h"

tree* create_parse_tree(){
    tree * tr = (tree *)malloc(sizeof(tree));
    tr->root = NULL;
    return tr;
}

node* create_node(char *term){
    node * p = (node *)malloc(sizeof(node));
    p->o = (object*)term;
    p->child_number = 0;
    return p;
}

void insert_node(node *p, node *child){
    p->childs[p->child_number++] = child;
}

void show_parse_tree(){
    print_parse_tree(tr->root, 0);
}

int isList(node *p){
    int l = (strcmp((char *)p->o,"<LINE_LIST>")==0);
    int d = (strcmp((char *)p->o,"<DECS>")==0);
    int v = (strcmp((char *)p->o,"<VAR_LIST>")==0);
    int f = (strcmp((char *)p->o,"<FIELD_DECLARE>")==0);
    int c = (strcmp((char *)p->o,"<FIELDS_CALL_DECLARE>")==0);
    
    if(l==1||d==1||v==1||f==1||c==1)
        return 1;
    else
        return 0;
}

void print_parse_tree(node *p, int depth){
    print_parse_node_term(p, depth);
    if(p->child_number>0){
        if(isList(p)){
            for(int i=(p->child_number-1); i >=0 ; i--){
                print_parse_tree(p->childs[i], depth+1);
                if(i!=0){
                    for(int j=0; j < depth+1; j++)
                        printf("    ");
                    printf(",\n");
                }
            }
            print_parse_node_term_end(p, depth);
        }
        else{
            for(int i=0; i < p->child_number; i++){
                print_parse_tree(p->childs[i], depth+1);
                if(i!=(p->child_number-1)){
                    for(int j=0; j < depth+1; j++)
                        printf("    ");
                    printf(",\n");
                }
            }
            print_parse_node_term_end(p, depth);
        }
    }
}

void print_parse_node_term(node *p, int depth){
    for(int i=0; i < depth; i++)
        printf("    ");
    if(p->child_number==0)
        printf("%s\n", (char*)p->o);
    else
        printf("%s {\n", (char*)p->o);
}

void print_parse_node_term_end(node *p, int depth){
    for(int i=0; i < depth; i++)
        printf("    ");
    printf("}\n");
}