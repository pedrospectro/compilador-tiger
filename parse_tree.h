#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_CHILD_NUMBER 1024

typedef void * object;

typedef struct node{
    object *o;
    int child_number;
    struct node* childs[MAX_CHILD_NUMBER];
}node;

typedef struct tree{
    struct node* root;
}tree;

tree *tr;

tree* create_parse_tree();
node* create_node(char *term);
void insert_node(node *p, node *child);
void show_parse_tree();
void print_parse_tree(node *p, int depth);
void print_parse_node_term(node *p, int depth);
void print_parse_node_term_end(node *p, int depth);
int isList(node *p);