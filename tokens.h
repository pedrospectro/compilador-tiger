#include <stdio.h>
#include <string.h>
#include "parse_tree.h"

#define MAX_TOKEN 2048
#define TAM_TOKEN 24

typedef enum tokens {
  t_var, t_function, t_if, t_then, t_else,
  t_while, t_do, t_let, t_in, t_end,
  
  t_num, t_id, t_literal,

  t_attr, t_scollum, t_comma, t_oparenthesis, t_cparenthesis,

  t_sum, t_minus, t_mul, t_div, 
  t_eq, t_diff, t_greater, t_less,t_greater_equal, t_less_equal,
  
  t_and, t_or,

  t_true, t_false,
  t_col, t_integer, t_string, t_boolean, t_float, t_for, t_to,
  t_break
} tokens;

tokens t;
char token_data[MAX_TOKEN][TAM_TOKEN];